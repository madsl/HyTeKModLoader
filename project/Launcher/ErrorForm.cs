﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using static LauncherConfiguration;

namespace Launcher
{
    public partial class ErrorForm : Form
    {
        object _error = null;
        string pbin = "$_az";
        public ErrorForm(object error)
        {
            InitializeComponent();
            this.Text = MODLOADER_NAME + " Error";
            label1.Text = MODLOADER_NAME_NOSPACES + " has encountered an error";
            label2.Text = @"An error caused " + MODLOADER_NAME_NOSPACES + " to stop working. A crash report is being uploaded to the " + MODLOADER_DEVTEAM_NAME + " developers. If you require immediate support, please visit " + DISCORD_INVITEURL + " and mention the details below in the #support channel.";
            UploadLogs(error);
        }

        async void UploadLogs(object error)
        {
            _error = error;
            string id = "#" + (DateTime.Now.Ticks - new DateTime(9999, 1, 1).Ticks).ToString("x");
            string ver = (string)Properties.Settings.Default["version"];
            string os = GetWindowsVersion();
            string p = "hjg@gGsmnn6pE9^Y@R4mBet8Wf5hxwK@";

            using (HttpClient httpclient = new HttpClient())
            {
                string flattenException = FlattenException((Exception)error);
                if (flattenException.Contains("Failed to find mono.dll in the target process"))
                {
                    return;
                }
                var values = new Dictionary<string, string>
                {
                    { "api_dev_key", "f29f535dfe313e5b024aba17be8d619e" },
                    { "api_option", "paste" },
                    { "api_paste_code", flattenException },
                };

                FormUrlEncodedContent content = new FormUrlEncodedContent(values);
                HttpResponseMessage response = await httpclient.PostAsync("https://pastebin.com/api/api_post.php", content).ConfigureAwait(true);
                content.Dispose();

                var responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(true);
                if (responseString.ToUpperInvariant().StartsWith("HTTPS://PASTEBIN.COM/", StringComparison.InvariantCulture))
                {
                    pbin = responseString;
                    using (HttpClient httpclient2 = new HttpClient())
                    {
                        var values2 = new Dictionary<string, string>
                        {
                            { "id", id },
                            { "ver", ver },
                            { "os", os },
                            { "pbin", pbin },
                            { "p", p },
                        };

                        FormUrlEncodedContent content2 = new FormUrlEncodedContent(values2);
                        HttpResponseMessage response2 = await httpclient2.PostAsync("https://" + SERVICE_FASTDL.ip + "/" + "errorreporter.php", content2).ConfigureAwait(true);
                        content2.Dispose();
                    }
                }

                progressBar.Style = ProgressBarStyle.Blocks;
                progressBar.Value = 1;
            }
        }

        public static string FlattenException(Exception exception)
        {
            var stringBuilder = new StringBuilder();

            while (exception != null)
            {
                stringBuilder.AppendLine(exception.Message);
                stringBuilder.AppendLine(exception.StackTrace);

                exception = exception.InnerException;
            }

            return stringBuilder.ToString();
        }

        [DllImport("kernel32.dll")]
        static extern IntPtr GetCurrentProcess();

        [DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
        static extern IntPtr GetModuleHandle(string moduleName);

        [DllImport("kernel32", CharSet = CharSet.Unicode)]
        static extern IntPtr GetProcAddress(IntPtr hModule, string procName);

        [DllImport("kernel32.dll")]
        static extern bool IsWow64Process(IntPtr hProcess, out bool wow64Process);

        public static bool Is64BitOperatingSystem()
        {
            // Check if this process is natively an x64 process. If it is, it will only run on x64 environments, thus, the environment must be x64.
            if (IntPtr.Size == 8)
                return true;
            // Check if this process is an x86 process running on an x64 environment.
            IntPtr moduleHandle = GetModuleHandle("kernel32");
            if (moduleHandle != IntPtr.Zero)
            {
                IntPtr processAddress = GetProcAddress(moduleHandle, "IsWow64Process");
                if (processAddress != IntPtr.Zero)
                {
                    bool result;
                    if (IsWow64Process(GetCurrentProcess(), out result) && result)
                        return true;
                }
            }
            // The environment must be an x86 environment.
            return false;
        }

        private static string HKLM_GetString(string key, string value)
        {
            try
            {
                RegistryKey registryKey = Registry.LocalMachine.OpenSubKey(key);
                return registryKey?.GetValue(value).ToString() ?? String.Empty;
            }
            catch
            {
                return String.Empty;
            }
        }

        public static string GetWindowsVersion()
        {
            string osArchitecture;
            try
            {
                osArchitecture = Is64BitOperatingSystem() ? "64-bit" : "32-bit";
            }
            catch (Exception)
            {
                osArchitecture = "32/64-bit (Undetermined)";
            }
            string productName = HKLM_GetString(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion", "ProductName");
            string csdVersion = HKLM_GetString(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion", "CSDVersion");
            string currentBuild = HKLM_GetString(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion", "CurrentBuild");
            if (!string.IsNullOrEmpty(productName))
            {
                return
                    $"{productName}{(!string.IsNullOrEmpty(csdVersion) ? " " + csdVersion : String.Empty)} {osArchitecture} (Build {currentBuild})";
            }
            return String.Empty;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Process.GetCurrentProcess().Kill();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (pbin.Length > 5)
            {
                Process.Start(pbin);
            }
            else
            {
                MessageBox.Show(_error.ToString(), "Crash Logs", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
