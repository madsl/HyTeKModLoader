﻿using System.Drawing;
using System.Threading.Tasks;

namespace Launcher
{
    public class DownloadsTab : LauncherTab
    {
        public override void Initialize()
        {
            panel = Main.Get().downloadPanel;
            panel.Location = new Point(-327, 40);
        }

        public async override void OnTabOpen()
        {
            base.OnTabOpen();
            Main.SettingsTab.OnTabClose();
            Main.ServicesTab.OnTabClose();
            while (panel.Location.X < 0 && IsOpened)
            {
                await Task.Delay(1);
                panel.Location = new Point(Main.Clamp(panel.Location.X + 40, -327, 0), 40);
            }
        }

        public async override void OnTabClose()
        {
            base.OnTabClose();
            while (panel.Location.X > -327 && !IsOpened)
            {
                await Task.Delay(1);
                panel.Location = new Point(Main.Clamp(panel.Location.X - 40, -327, 0), 40);
            }
        }
    }
}