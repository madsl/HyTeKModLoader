﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("HyTeK Mod Loader Launcher")]
[assembly: AssemblyDescription("This software manage the ModLoader!")]
[assembly: AssemblyCompany("https://www.hytekgames.net")]
[assembly: AssemblyProduct("HyTeK Mod Loader Launcher")]
[assembly: AssemblyCopyright("Copyright HyTeKGames © 2020")]
[assembly: AssemblyTrademark("HyTeKGames")]
[assembly: Guid("2d8e2fea-f80f-44a5-b87a-c6ba8196b4cd")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
