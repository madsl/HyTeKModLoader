﻿using GHML;
using HarmonyLib;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace GHML
{
    [HarmonyPatch(typeof(Harmony))]
    [HarmonyPatch("UnpatchAll")]
    static class HarmonyUnpatchAllFix
    {
        static bool Prefix(Harmony __instance, string harmonyID)
        {
            if (harmonyID == null)
            {
                string id = __instance.Id;
                if (id != null && id != "" && !string.IsNullOrWhiteSpace(id))
                {
                    Debug.LogWarning("[HarmonyFix] Warning! UnpatchAll() has been called with no id! Trying to call it on id \"" + id + "\"");
                    __instance.UnpatchAll(id);
                    return false;
                }
                else
                {
                    Debug.LogError("[HarmonyFix] Error! UnpatchAll() has been called with no id and no id could be found! Cancelled invoke.");
                    return false;
                }
            }
            return true;
        }
    }

    [HarmonyPatch(typeof(InputsManager))]
    [HarmonyPatch("GetActionValue")]
    [HarmonyPatch(new Type[] { typeof(int) })]
    class Patch_InputsManager_GetActionValue
    {
        static bool Prefix(InputsManager __instance, int action, ref float __result)
        {
            if (InternalGHAPI.isCursorShown)
            {
                __result = 0f;
                return false;
            }
            else
            {
                return true;
            }
        }
    }

    [HarmonyPatch(typeof(InputsManager))]
    [HarmonyPatch("OnAction")]
    class Patch_InputsManager_OnAction
    {
        static bool Prefix(InputsManager __instance)
        {
            return !InternalGHAPI.isCursorShown;
        }
    }

    [HarmonyPatch(typeof(CursorManager))]
    [HarmonyPatch("SetCursorRequestsCount")]
    class Patch_CursorManager_SetCursorRequestsCount
    {
        static bool Prefix(CursorManager __instance, int count)
        {
            Traverse.Create(__instance).Field("m_ShowCursorRequests").SetValue(count);
            return false;
        }
    }
}