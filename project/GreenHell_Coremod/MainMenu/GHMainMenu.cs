﻿using HMLLibrary;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace GHML
{
    public class GHMainMenu : MonoBehaviour
    {
        public static GHMainMenu instance;
        public static bool IsOpen = true;
        public static GameObject VersionText;
        private string CurrentPage;
        private static GameObject pages;
        private static Dictionary<string, MenuPage> menuPages = new Dictionary<string, MenuPage>();
        private static GameObject LeftPagesButtons;
        private static Text OnlineUsers;
        private bool initialized = false;

        public async void Initialize()
        {
            instance = this;
            pages = transform.Find("BG").Find("GHMLMainMenu_PageZone").gameObject;
            foreach (Transform p in pages.transform)
            {
                p.gameObject.SetActive(true);
            }
            OnlineUsers = transform.Find("BG").Find("LeftBar").Find("GHMLOnlineUsersCounter").GetComponent<Text>();
            LeftPagesButtons = transform.Find("BG").Find("LeftBar").Find("GHMLMainMenu_Slots").gameObject;
            VersionText = Instantiate(await HLib.bundle.TaskLoadAssetAsync<GameObject>("VersionText"), Vector3.zero, Quaternion.identity);
            VersionText.GetComponentInChildren<Text>().text = @"GreenHellModLoader <color=#36d1a8>v" + GHML_Main.modloader_config.coreVersion + @"</color>
Press <color=#36d1a8>F9</color> to open the main menu
Press <color=#36d1a8>F10</color> to open the console";
            DontDestroyOnLoad(VersionText);

            LeftPagesButtons.transform.Find("GHMLMainMenuLButton_Home").GetComponent<Button>().onClick.AddListener(() => { ChangeMenu("Home"); });
            LeftPagesButtons.transform.Find("GHMLMainMenuLButton_ModManager").GetComponent<Button>().onClick.AddListener(() => { ChangeMenu("ModManager"); });
            LeftPagesButtons.transform.Find("GHMLMainMenuLButton_Servers").GetComponent<Button>().onClick.AddListener(() => { ChangeMenu("Servers"); });
            LeftPagesButtons.transform.Find("GHMLMainMenuLButton_Settings").GetComponent<Button>().onClick.AddListener(() => { ChangeMenu("Settings"); });
            LeftPagesButtons.transform.Find("GHMLMainMenuLButton_Credits").GetComponent<Button>().onClick.AddListener(() => { ChangeMenu("Credits"); });
            transform.Find("BG").Find("TopBar").Find("GHMLMainMenuCloseBtn").GetComponent<Button>().onClick.AddListener(CloseMenu);

            menuPages.Add("Home", pages.transform.Find("Home").gameObject.AddComponent<HomePage>());
            menuPages.Add("ModManager", pages.transform.Find("ModManager").gameObject.AddComponent<ModManagerPage>());
            menuPages.Add("Servers", pages.transform.Find("Servers").gameObject.AddComponent<ServersPage>());
            menuPages.Add("Settings", pages.transform.Find("Settings").gameObject.AddComponent<SettingsPage>());
            menuPages.Add("Credits", pages.transform.Find("Credits").gameObject.AddComponent<CreditsPage>());

            menuPages.ToList().ForEach((p) => p.Value.Initialize());
            initialized = true;
        }

        public static void CloseMenu()
        {
            ModManagerPage.RefreshModsStates();
            IsOpen = false;
            if (!GHConsole.isOpen)
            {
                InternalGHAPI.GHInternal_ShowCursor(false);
            }
            instance.StartCoroutine(instance.CloseMenuLate());
        }

        public void OpenMenu()
        {
            IsOpen = true;
            ChangeMenu(CurrentPage);
            GetComponent<Canvas>().enabled = true;
            GetComponent<Animation>().Play("Menu_Open");
            if (!GHConsole.isOpen)
            {
                InternalGHAPI.GHInternal_ShowCursor(true);
            }
            ModManagerPage.RefreshModsStates();
        }

        private async void UpdateOnlineUsers()
        {
            try
            {
                string onlineUsers = await new WebClient().DownloadStringTaskAsync("https://fastdl.greenhellmodding.com/data/clients.txt");
                int amount = int.Parse(onlineUsers);
                OnlineUsers.text = "Online Users : " + amount;
            }
            catch { }
        }

        private void LateUpdate()
        {
            if (!initialized) { return; }
            if (SceneManager.GetActiveScene().name == "MainMenu" && LoadingScreen.Get().m_State == LoadingScreenState.None)
            {
                VersionText.GetComponent<Canvas>().enabled = true;
            }
            else
            {
                VersionText.GetComponent<Canvas>().enabled = false;
            }

            if (!SettingsPage.EnableMenuInGame && SceneManager.GetActiveScene().name != "MainMenu")
            {
                GetComponent<Canvas>().enabled = false;
                IsOpen = false;
                return;
            }

            if (Input.GetKeyDown(GHML_Main.MenuKey))
            {
                IsOpen = !IsOpen;
                if (IsOpen)
                {
                    OpenMenu();
                }
                else
                {
                    CloseMenu();
                }
            }
            else if (Input.GetKeyDown(KeyCode.Escape) && IsOpen)
            {
                CloseMenu();
            }
        }

        private IEnumerator CloseMenuLate()
        {
            GetComponent<Animation>().Play("Menu_Close");
            yield return new WaitForSeconds(0.10f);
            GetComponent<Canvas>().enabled = false;
        }

        public static void ChangeMenu(string menuname)
        {
            instance.UpdateOnlineUsers();
            foreach (Transform p in pages.transform)
            {
                if (p.gameObject.activeSelf)
                {
                    if (menuPages.ContainsKey(p.name))
                    {
                        menuPages[p.name].OnPageOpen();
                    }
                }
                p.gameObject.SetActive(false);
            }
            if (menuPages.ContainsKey(menuname))
            {
                menuPages[menuname].OnPageOpen();
            }
            instance.StartCoroutine(instance.ChangePage(menuname));
        }

        private IEnumerator ChangePage(string menuname)
        {
            yield return new WaitForEndOfFrame();
            foreach (Transform b in LeftPagesButtons.transform)
            {
                b.GetComponent<Button>().interactable = true;
            }
            pages.GetComponent<CanvasGroup>().alpha = 0;
            GameObject page = pages.transform.Find(menuname).gameObject;
            Button button = LeftPagesButtons.transform.Find("GHMLMainMenuLButton_" + menuname).gameObject.GetComponent<Button>();

            if (page != null)
            {
                page.SetActive(true);
                pages.GetComponent<Animation>().Play();
                button.interactable = false;
                CurrentPage = menuname;
            }
        }
    }
}